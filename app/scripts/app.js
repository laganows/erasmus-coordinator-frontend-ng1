'use strict';

/**
 * @ngdoc overview
 * @name erasmusCoordinatorFrontendNg1App
 * @description
 * # erasmusCoordinatorFrontendNg1App
 *
 * Main module of the application.
 */
angular
  .module('erasmusCoordinatorFrontendNg1App', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.grid',
    'ui.grid.pagination'
  ])
  .config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
  }])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/offers.html',
        controller: 'OffersCtrl',
        controllerAs: 'offers'
      })
      .when('/agreements', {
        templateUrl: 'views/agreements.html',
        controller: 'AgreementsCtrl',
        controllerAs: 'agreements'
      })
      .when('/applications/:offerId', {
        templateUrl: 'views/applications.html',
        controller: 'ApplicationsCtrl',
        controllerAs: 'applications'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .constant('serverAddress', 'http://localhost:8080');

