'use strict';

angular.module('erasmusCoordinatorFrontendNg1App')
  .controller('ApplicationsCtrl', function (serverAddress, $http, $scope, $routeParams) {

    $http.get(serverAddress + '/api/offers/' + $routeParams.offerId + "/applications")
      .then(function (response) {
        $scope.applications = response.data;
        console.info(response.data);
      }, function () {
        console.info('Events not found');
      });

    $scope.apply = function () {
      var postObject = {};
      var applier = {};
      applier.studiesDegree = "SUPPLEMENTARY";
      applier.typeOfStudies = "EVENING_STUDIES";
      postObject.universityOfferId = $routeParams.offerId;
      postObject.applierId = "59f79e94ed87b853f0c9f8f6";
      postObject.applierData = applier;
      console.log(postObject);

      $http({
        url: serverAddress + "/api/applications/",
        method: "POST",
        data: postObject
      })
        .then(function (response) {
          console.log($scope.applications);
          $scope.applications.push(response.data);
          console.log($scope.applications);
        }, function () {
          console.info('Error with adding new application');
        });
    };

  });
