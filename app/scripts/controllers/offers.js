'use strict';

angular.module('erasmusCoordinatorFrontendNg1App')
  .controller('OffersCtrl', function (serverAddress, $http, $scope) {

    $http.get(serverAddress + '/api/offers/')
      .then(function (response) {
        $scope.offers = response.data;
        console.info(response.data);
      }, function () {
        console.info('Events not found');
      });

    $scope.addOffer = function (country, duration, erasmusCode, university, year, vacancies) {
      var postObject = {};
      postObject.country = country;
      postObject.duration = duration;
      postObject.erasmusCode = erasmusCode;
      postObject.erasmusCoordinatorId = "59f79e94ed87b853f0c9f8f6";
      postObject.departmentCoordinatorId = "59f79e94ed87b853f0c9f8f6";
      postObject.university = university;
      postObject.universityApplicationIds = [];
      postObject.year = year;
      postObject.vacancies = vacancies;

      console.log(postObject);

      $http({
        url: serverAddress + "/api/offers/",
        method: "POST",
        data: postObject
      })
        .then(function (response) {
          $('#addNewOfferModal').modal('hide');
          console.log($scope.offers);
          $scope.offers.push(response.data);
          console.log($scope.offers);
        }, function () {
          console.info('Error with adding new offer');
        });
    };

  });
