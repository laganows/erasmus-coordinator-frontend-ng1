'use strict';

angular.module('erasmusCoordinatorFrontendNg1App')
  .controller('AgreementsCtrl', function (serverAddress, $http, $scope, $routeParams) {
    var agreementServerAddress = 'http://localhost:8081'

    var paginationOptions = {
      pageNumber: 1,
      pageSize: 5,
      sort: null
    };

    getStudents(
      paginationOptions.pageNumber,
      paginationOptions.pageSize).then(function (data) {
      console.log(data)
      $scope.gridOptions.data = data.data._embedded.agreements;
      $scope.gridOptions.totalItems = data.data.page.totalElements;
    });

    $scope.gridOptions = {
      paginationPageSizes: [5, 10, 20],
      paginationPageSize: paginationOptions.pageSize,
      enableColumnMenus: false,
      useExternalPagination: true,
      columnDefs: [
        {name: 'code'},
        {name: 'coordinator'},
        {name: 'department'},
        {name: 'duration'},
        {name: 'endYear'},
        {name: 'erasmusCoordinator'},
        {name: 'startYear'},
        {name: 'universityName'},
        {name: 'vacancies'}
      ],
      onRegisterApi: function (gridApi) {
        $scope.gridApi = gridApi;
        gridApi.pagination.on.paginationChanged(
          $scope,
          function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            getStudents(newPage, pageSize)
              .then(function (data) {
                $scope.gridOptions.data = data.data._embedded.agreements;
                $scope.gridOptions.totalItems = data.data.page.totalElements;
              });
          });
      }
    }

    function getStudents(pageNumber, size) {
      pageNumber = pageNumber > 0 ? pageNumber - 1 : 0;
      return $http({
        method: 'GET',
        url: agreementServerAddress + '/agreements?page=' + pageNumber + '&size=' + size
      });
    }

  });
